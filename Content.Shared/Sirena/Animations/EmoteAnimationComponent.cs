using Content.Shared.Actions;
using Content.Shared.Actions.ActionTypes;
using Robust.Shared.GameStates;
using Robust.Shared.Serialization;

namespace Content.Shared.Sirena.Animations;

/// <summary>
/// </summary>
public partial class EmoteFlipActionEvent : InstantActionEvent { };

/// <summary>
/// </summary>
public partial class EmoteJumpActionEvent : InstantActionEvent { };

/// <summary>
/// </summary>
public partial class EmoteTurnActionEvent : InstantActionEvent { };

[RegisterComponent]
[NetworkedComponent]
public partial class EmoteAnimationComponent : Component
{
    [ViewVariables(VVAccess.ReadWrite)]
    public string AnimationId = "none";

    public InstantAction? FlipAction;
    public InstantAction? JumpAction;
    public InstantAction? TurnAction;

    [Serializable, NetSerializable]
    public class EmoteAnimationComponentState : ComponentState
    {
        public string AnimationId { get; init; }

        public EmoteAnimationComponentState(string animationId)
        {
            AnimationId = animationId;
        }
    }
}
